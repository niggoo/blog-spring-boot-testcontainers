package io.auroria.book;

import io.auroria.book.api.BookApi;
import io.auroria.book.domain.BookDTO;
import io.auroria.book.domain.NewBookDTO;
import io.auroria.book.domain.NewBookRatingDTO;
import io.auroria.book.impl.DefaultBookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
class BookApplicationTest {

    @Autowired
    DefaultBookService bookService;

    @Test
    public void addBookViaService() {
        var author = "Josh Long, Kenny Bastani";
        var book = new NewBookDTO("Cloud Native Java", author);
        var resultBook = bookService.addBook(book);

        assertThat(resultBook.author()).isEqualTo(author);
        assertThat(resultBook.id()).isNotNull();
    }


    @Container
    static GenericContainer redis =
            new GenericContainer(DockerImageName.parse("redis:7"))
                    .withExposedPorts(6379);

    @DynamicPropertySource
    static void redisProperties(DynamicPropertyRegistry registry) {
        redis.start();
        registry.add("spring.redis.host", redis::getHost);
        registry.add("spring.redis.port", redis::getFirstMappedPort);
    }

    @Autowired
    BookApi bookApi;

    @Test
    public void addBookViaApi() {
        var author = "Betsy Beyer, Chris Jones, Jennifer Petoff " +
                "and Niall Richard Murphy";
        var book = new NewBookDTO("Site Reliability Engineering", author);
        var resultBook = bookApi.addBook(book);

        assertThat(resultBook.author()).isEqualTo(author);
    }

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void addRatingViaRestTemplate() {
        restTemplate.postForEntity("/books/1/rating",
                new NewBookRatingDTO(10), BookDTO.class);
        restTemplate.postForEntity("/books/2/rating",
                new NewBookRatingDTO(8), BookDTO.class);
        var response = restTemplate.postForEntity("/books/1/rating",
                new NewBookRatingDTO(2), BookDTO.class);

        assertThat(response.getBody().averageRating()).isEqualTo(6);
    }

    @Test
    public void getAllBooksREST() {
        var response = restTemplate.exchange("/books", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<BookDTO>>() {
                });

        assertThat(response.getBody().size()).isEqualTo(2);
    }
}