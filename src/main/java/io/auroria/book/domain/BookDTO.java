package io.auroria.book.domain;

import java.io.Serializable;

public record BookDTO(
        Long id,
        String name,
        String author,
        double averageRating
) implements Serializable {
}
