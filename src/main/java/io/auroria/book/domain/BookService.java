package io.auroria.book.domain;

import java.util.List;

public interface BookService {
    BookDTO addBook(NewBookDTO newBookDTO);

    BookDTO addRating(Long bookId, NewBookRatingDTO newBookRatingDTO);

    List<BookDTO> getBooks();
}
