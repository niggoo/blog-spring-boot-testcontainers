package io.auroria.book.impl;

import io.auroria.book.domain.BookDTO;
import io.auroria.book.domain.BookService;
import io.auroria.book.domain.NewBookDTO;
import io.auroria.book.domain.NewBookRatingDTO;
import io.auroria.book.persistence.BookRepository;
import io.auroria.book.persistence.RatingRepository;
import io.auroria.book.persistence.entity.Book;
import io.auroria.book.persistence.entity.Rating;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DefaultBookService implements BookService {

    private final BookRepository bookRepository;
    private final RatingRepository ratingRepository;

    @Override
    public BookDTO addBook(NewBookDTO newBookDTO) {
        var newBook = new Book(null, newBookDTO.getName(),
                newBookDTO.getAuthor());
        var savedBook = bookRepository.save(newBook);

        return convertToBookDTO(savedBook, Collections.emptyList());
    }

    @Override
    public BookDTO addRating(Long bookId,
                             NewBookRatingDTO newBookRatingDTO) {
        ratingRepository.save(new Rating(null, bookId,
                newBookRatingDTO.getRating()));

        var book = bookRepository.getReferenceById(bookId);
        var ratings = ratingRepository.findByBookId(bookId);

        return convertToBookDTO(book, ratings);
    }

    @Override
    public List<BookDTO> getBooks() {
        var books = bookRepository.findAll();
        var bookIds = books.stream()
                .map(Book::getId)
                .collect(Collectors.toList());

        var ratings = ratingRepository.findByBookIdIn(bookIds);

        return books.stream()
                .map(b -> {
                    var bookRatings = ratings.stream()
                            .filter(r -> r.getBookId().equals(b.getId()))
                            .collect(Collectors.toList());

                    return convertToBookDTO(b, bookRatings);
                })
                .collect(Collectors.toList());
    }

    private static BookDTO convertToBookDTO(Book book, List<Rating> ratings) {
        var averageRating = ratings.stream()
                .mapToDouble(Rating::getRating)
                .average()
                .orElse(0);

        return new BookDTO(book.getId(), book.getName(), book.getAuthor(), averageRating);
    }
}
