package io.auroria.book.api;

import io.auroria.book.domain.BookDTO;
import io.auroria.book.domain.BookService;
import io.auroria.book.domain.NewBookDTO;
import io.auroria.book.domain.NewBookRatingDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("books")
@RequiredArgsConstructor
public class BookApi {
    private final BookService bookService;

    @PostMapping
    @CacheEvict(value = "books", allEntries = true)
    public BookDTO addBook(@RequestBody NewBookDTO newBookDTO) {
        return bookService.addBook(newBookDTO);
    }

    @PostMapping("{bookId}/rating")
    @CacheEvict(value = "books", allEntries = true)
    public BookDTO addRating(@PathVariable("bookId") Long bookId,
                             @RequestBody NewBookRatingDTO newBookRatingDTO) {
        return bookService.addRating(bookId, newBookRatingDTO);
    }

    @GetMapping
    @Cacheable(value = "books")
    public List<BookDTO> getBooks() {
        return bookService.getBooks();
    }
}
